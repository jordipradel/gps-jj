# GPS

Projecte de partida per al laboratori de GPS de la FIB, curs de primavera de 2015

## Crear el repositori del vostre grup

1. Seleccioneu l'opció Fork del menú de l'esquerra (de vegades "amagada" dins la icona dels punts suspensius).
2. Poseu com a nom del repositori: "gps-jj-2015t-$nomGrup"

    - $nomGrup és el nom del vostre grup tal com el vau enviar per correu
    - $nomGrup no ha de tenir espais en blanc
    - Dins $nomGrup feu servir camelCase i no altres_maneres-de-separar_les_parts
    - Exemple: `gps-jj-2015t-agileRocksOhYeah`

3. Marqueu el repositori com a privat
4. Marqueu l'opció "Wiki"
5. Premeu acceptar
6. Compartiu el repositori amb els vostres companys de grup
	- Des de https://bitbucket.org/$username/$nomRepo
	- Seleccioneu Settings (icona d'engranatge de l'esquerra)
	- Seleccioneu Access Management
	- Afegiu "jordipradel" amb drets d'escriptura (no d'administració)
	- Afegiu cada un dels companys amb drets d'administració

## Crear el backlog

1. Des de https://bitbucket.org/$username/$nomRepo
2. Aneu a la wiki del projecte (icona d'un full a l'esquerra)
3. Creeu una nova pàgina anomenada "Backlog"
4. Ompliu-la seguint l'exemple de backlog d'[aquí](https://dl.dropboxusercontent.com/u/60774636/BacklogExample.md)

## Preparar el vostre fitxer README.md

1. Des de https://bitbucket.org/$username/$nomRepo o des del vostre clon del repositori
2. Editeu aquest fitxer README.md i esborreu els apartats d'instruccions:
	- "Crear el repositori del vostre grup"
	- "Crear el backlog"
	- "Preparar el vostre fitxer README.md"
3. Ompliu l'apartat "Components del grup" seguint els exemples (i esborrant-los)
4. Actualitzeu l'enllaç al backlog per a que apunti a la vostra pàgina de backlog


## Components del grup

- John Doe Cabrera (43444546D) - johm.doe@est.upc.edu
- Joe Bar Farré (44454647F) - joe.bar@est.upc.edu

## Enllaços

- [Backlog](https://bitbucket.org/jordipradel/gps-jj/wiki/Backlog)